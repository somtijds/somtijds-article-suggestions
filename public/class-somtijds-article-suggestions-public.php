<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.somtijds.nl
 * @since      0.5.0
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/public
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Somtijds_Article_Suggestions_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.5.0
	 * @access   private
	 * @var      string    $S_A_S    The ID of this plugin.
	 */
	private $S_A_S;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.5.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.5.0
	 * @param      string    $S_A_S       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $S_A_S, $version ) {

		$this->S_A_S = $S_A_S;
		$this->version = $version;
		$this->init();

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    0.5.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Somtijds_Article_Suggestions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Somtijds_Article_Suggestions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->S_A_S, plugin_dir_url( __FILE__ ) . 'css/somtijds-article-suggestions-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    0.5.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Somtijds_Article_Suggestions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Somtijds_Article_Suggestions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		$params = array ( 'ajaxurl' => admin_url( 'admin-ajax.php' ) );
		wp_enqueue_script( $this->S_A_S, plugin_dir_url( __FILE__ ) . 'js/somtijds-article-suggestions-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->S_A_S, 'sasparams', $params );		

	}

	/**
	 * Initiate
	 *
	 * @since    0.5.0
	 */
	public function init() {

		add_shortcode( 'sas_form', array( $this, 'publish_form' ) );
		
	}

	/**
	 * Publish Form
	 *
	 * @since    0.5.0
	 */
	public function publish_form() {
		
		$nonce = wp_create_nonce( 'sas_form_process' );

		$output = '';
		$output .= '<form class="sas_form" id="sas_form" action="'
			. esc_url( get_site_url( null, 'admin-ajax.php' ) ) 
			.'" method="POST">';
		$output .= '<input type="hidden" name="action" value="sas_form_process">';
		$output .= '<input type="hidden" name="sas_form__nonce" value="' . $nonce . '">';
		$output .= '<h3 class="sas_form__title">' 
			. __( 'Artikel einfügen', $this->S_A_S->plugin_name ) 
			. '</h3>';
		$output .= '<label for="sas_form__input">' 
			. __( 'Titel:', $this->S_A_S->plugin_name ) 
			. '<input name="sas_form__input" type="text" id="sas_form__input" required minlength="1" maxlength="100"></label>';
		$output .= '<button id="sas_form__submit" class="sas_form__submit">' 
			. __( 'Einfügen', $this->S_A_S->plugin_name ) 
			. '</button>';
		$output .= '</form>';
		$output .= '<div class="sas_form__errors"></div>';
		$output .= '<ul class="sas_form__submissions"></ul>';

		return $output;

	}

public function send_response( $response ) {

	if ( empty( $response ) ) {
		echo "Response is empty";
		wp_die();
	} 

	echo json_encode( $response );
	wp_die();

}

	/**
	 * Process Form submission
	 *
	 * @since    0.5.0
	 */
	public function process_form_submission() {

		$response = array(
			'error' => false,
		);

		if ( ! wp_verify_nonce( $_REQUEST['sas_form__nonce'], 'sas_form_process' ) ) {
			$response[ 'error' ] = true;
			$response[ 'error_message' ] = __( 'Wir haben Ihre Daten leider nicht richtig erhalten', $this->S_A_S->plugin_name );

			$this->send_response( $response );
			return;
		}

		$post_title_suggestion = sanitize_text_field( $_REQUEST[ 'sas_form__input' ] );

		if ( ! empty( $post_title_suggestion ) ) {
			$post = wp_insert_post( array( 
				'post_title' => $post_title_suggestion,
				'post_status' => 'draft',
			) );
		}

		if ( ! isset( $post ) ) {

			$response[ 'error' ] = true;
			$response[ 'error_message' ] = __( 'Wir haben Ihren Beitrag leider nicht richtig speichern können', $this->S_A_S->plugin_name );

			$this->send_response( $response );
			return;
		}

		$post_terms = wp_set_post_terms( $post, __( 'From Suggestions Form', $this->S_A_S->plugin_name ), 'post_tag' );
		
		$response[ 'data' ] = array(
			'post_date' => get_the_date( 'd.m.Y', $post ),
			'post_title' => get_the_title( $post ),
		);

		$this->send_response( $response );
	}
}
