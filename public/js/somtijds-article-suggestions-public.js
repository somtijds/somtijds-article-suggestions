(function( $ ) {

	'use strict';

	$( document ).ready( function() {
		
		function sasGetForm() {
			return document.getElementById('sas_form');
		}
	
		function sasIsValid() {
			if ( sasGetForm().checkValidity() ) {
				return true;
			}
		}

		$('#sas_form').submit( function( e ) {
			
			e.preventDefault();

			if ( ! sasIsValid() ) {
				return;
			}
			
			var $sasFormData = $(this).serialize();

			$sasFormData = $sasFormData+'&ajaxrequest=true&submit=Submit+Form';

			var $sasTitleVal = $('#sas_form__input').val();
			
			$.ajax({
				url: sasparams.ajaxurl, 
				type: 'post',
				data: $sasFormData
			})
			.done( function( response ) {
				var decoded = $.parseJSON(response );
				if ( undefined == decoded.data ) {
					$('.sas_form__errors').html(decoded.error_message);
				} else {
					$('.sas_form__submissions').append(
						'<li><h3>' + decoded.data.post_title + '</h3><span class="date">' + decoded.data.post_date + '</span><li>'
					);
				}
			})
            .fail( function() {
				console.error( 'Something went wrong' );
                
            })
            .always( function() {
                e.target.reset();
            });
		})

	
	})


})( jQuery );
