<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.somtijds.nl
 * @since      0.5.0
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.5.0
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/includes
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Somtijds_Article_Suggestions_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.5.0
	 */
	public static function activate() {

	}

}
