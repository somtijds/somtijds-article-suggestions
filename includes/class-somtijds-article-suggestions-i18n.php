<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.somtijds.nl
 * @since      0.5.0
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.5.0
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/includes
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Somtijds_Article_Suggestions_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.5.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'somtijds-article-suggestions',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
