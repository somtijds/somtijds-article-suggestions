<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.somtijds.nl
 * @since             0.5.0
 * @package           Somtijds_Article_Suggestions
 *
 * @wordpress-plugin
 * Plugin Name:       Somtijds Article Suggestions
 * Plugin URI:        https://bitbucket.org/somtijds/somtijds-article-suggestions/
 * Description:       This plugin allows for the creation of an article suggestion box by using the [sas_form] shortcode
 * Version:           0.5.1
 * Author:            Willem Prins
 * Author URI:        https://www.somtijds.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       somtijds-article-suggestions
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 0.5.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '0.5.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-somtijds-article-suggestions-activator.php
 */
function activate_somtijds_article_suggestions() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-somtijds-article-suggestions-activator.php';
	Somtijds_Article_Suggestions_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-somtijds-article-suggestions-deactivator.php
 */
function deactivate_somtijds_article_suggestions() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-somtijds-article-suggestions-deactivator.php';
	Somtijds_Article_Suggestions_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_somtijds_article_suggestions' );
register_deactivation_hook( __FILE__, 'deactivate_somtijds_article_suggestions' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-somtijds-article-suggestions.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.5.0
 */
function run_somtijds_article_suggestions() {

	$plugin = new Somtijds_Article_Suggestions();
	$plugin->run();

}
run_somtijds_article_suggestions();
