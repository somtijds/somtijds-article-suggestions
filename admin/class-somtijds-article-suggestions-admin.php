<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.somtijds.nl
 * @since      0.5.0
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/admin
 * @author     Willem Prins <willem@somtijds.nl>
 */
class Somtijds_Article_Suggestions_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    0.5.0
	 * @access   private
	 * @var      string    $S_A_S    The ID of this plugin.
	 */
	private $S_A_S;

	/**
	 * The version of this plugin.
	 *
	 * @since    0.5.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    0.5.0
	 * @param      string    $S_A_S       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $S_A_S, $version ) {

		$this->plugin_name = $S_A_S;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    0.5.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Somtijds_Article_Suggestions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Somtijds_Article_Suggestions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/somtijds-article-suggestions-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    0.5.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Somtijds_Article_Suggestions_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Somtijds_Article_Suggestions_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/somtijds-article-suggestions-admin.js', array( 'jquery' ), $this->version, false );

	}

}
