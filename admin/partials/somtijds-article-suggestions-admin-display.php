<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.somtijds.nl
 * @since      0.5.0
 *
 * @package    Somtijds_Article_Suggestions
 * @subpackage Somtijds_Article_Suggestions/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
