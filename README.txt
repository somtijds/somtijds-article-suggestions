=== Plugin Name ===
Contributors: somtijds
Donate link: http://www.somtijds.nl/
Tags: comments, spam
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

By using the [sas_form] shortcode, you can add a form to any post or page!

== Installation ==

1. Upload the `somtijds-article-suggestions` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. This screen shot shows the form on the frontend. The plugin will adapt to your form's default CSS!

== Changelog ==

0.5.1: Add two minor fixes to allow users who are logged in to use the form as well. 